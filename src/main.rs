#[macro_use]
extern crate serde_derive;

use mcai_worker_sdk::prelude::*;

mod message;

pub mod built_info {
  include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

fn default_template_mode() -> String {
  "string".to_string()
}

fn default_output_mode() -> String {
  "json".to_string()
}

#[derive(Clone, Default, Debug, Deserialize, JsonSchema)]
pub struct JsonTransformParameters {
  destination_path: String,
  source_paths: Vec<String>,
  template: String,
  #[serde(default = "default_template_mode")]
  template_mode: String,
  #[serde(default = "default_output_mode")]
  output_mode: String,
}

#[derive(Debug, Default)]
struct JsonTransformWorker {}

impl McaiWorker<JsonTransformParameters> for JsonTransformWorker {
  fn get_name(&self) -> String {
    "Json transform".to_string()
  }

  fn get_short_description(&self) -> String {
    "Transform json file(s) into another json based on template".to_string()
  }

  fn get_description(&self) -> String {
    r#"This worker enables the transformation of a json into an another json.
    The template is based on jq syntax to provide a very generic transformation tool.
    Input can be a single file to generate one transformed file.
    In case of multiple files is passed, a merged can be performed."#
      .to_string()
  }

  fn get_version(&self) -> Version {
    Version::parse(built_info::PKG_VERSION).expect("unable to locate Package version")
  }

  fn process(
    &self,
    channel: Option<McaiChannel>,
    parameters: JsonTransformParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    message::process(channel, parameters, job_result)
  }
}

fn main() {
  let message_event = JsonTransformWorker::default();
  start_worker(message_event);
}
