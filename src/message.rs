use crate::JsonTransformParameters;
use jq_rs::JqProgram;
use jxon::{json_to_xml, xml_to_json};
use mcai_worker_sdk::{
  job::{JobResult, JobStatus},
  prelude::{debug, info},
  McaiChannel, MessageError,
};
use std::{fs, path::Path};
use xmltree::Element;

pub fn process(
  _channel: Option<McaiChannel>,
  parameters: JsonTransformParameters,
  job_result: JobResult,
) -> Result<JobResult, MessageError> {
  let result = match parameters.template_mode.as_str() {
    "string" => jq_process(job_result.clone(), &parameters, false),
    "file" => jq_process(job_result.clone(), &parameters, true),
    mode => Err(MessageError::ProcessingError(
      job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&format!("Mode {:?} not supported.", mode)),
    )),
  };

  result
    .map(|_| job_result.clone().with_status(JobStatus::Completed))
    .map_err(|error| error)
}

fn jq_process(
  job_result: JobResult,
  parameters: &JsonTransformParameters,
  is_source_path_template: bool,
) -> Result<(), MessageError> {
  let mut program = get_filter_program(job_result.clone(), parameters, is_source_path_template)?;
  let source_paths = parameters.source_paths.clone();
  let destination_path = parameters.destination_path.clone();
  let output_mode = parameters.output_mode.clone();

  for source_path in source_paths {
    let input_path = Path::new(&source_path);
    let output_path = Path::new(&destination_path);

    info!("{}", format!("Run jq program on '{}'.", source_path));

    if !input_path.is_file() {
      let result = job_result
        .with_status(JobStatus::Error)
        .with_message(&format!("No such file: {}", source_path));
      return Err(MessageError::ProcessingError(result));
    }

    info!("{}", format!("Parse content of '{}'.", source_path));

    let file_content = read_source_content(job_result.clone(), input_path)?;

    debug!("Run jq program.");

    let output_content = &program.run(&file_content.to_string()).map_err(|e| {
      let result = job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&format!("Unable to process JQ: {}", e));
      MessageError::ProcessingError(result)
    })?;

    debug!("Write jq program result.");
    info!("{}", output_mode);

    write_destination_content(
      job_result.clone(),
      output_path,
      output_content,
      &output_mode,
    )?;
  }

  Ok(())
}

fn get_filter_program(
  job_result: JobResult,
  parameters: &JsonTransformParameters,
  is_source_path_template: bool,
) -> Result<JqProgram, MessageError> {
  let template_source = parameters.template.clone();

  let template_content = if is_source_path_template {
    fs::read_to_string(template_source).map_err(|e| {
      let result = job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&format!("Unable to read template file: {}", e));
      MessageError::ProcessingError(result)
    })?
  } else {
    template_source
  };

  info!("Compile template as a jq program");

  let program = jq_rs::compile(&template_content).map_err(|e| {
    let result = job_result
      .clone()
      .with_status(JobStatus::Error)
      .with_message(&format!("Unable to read file: {}", e));
    MessageError::ProcessingError(result)
  })?;
  Ok(program)
}

fn read_source_content(job_result: JobResult, path: &Path) -> Result<String, MessageError> {
  let raw_content = fs::read_to_string(path).map_err(|e| {
    let result = job_result
      .clone()
      .with_status(JobStatus::Error)
      .with_message(&format!("Unable to read file: {}", e));
    MessageError::ProcessingError(result)
  })?;

  let content = if Element::parse(raw_content.as_bytes()).is_ok() {
    let json = xml_to_json(&raw_content).map_err(|e| {
      let result = job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&format!("Unable to convert XML input to JSON: {}", e));
      MessageError::ProcessingError(result)
    })?;

    serde_json::to_string(&json).map_err(|e| {
      let result = job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&format!("Could not serialize input to JSON: {}", e));
      MessageError::ProcessingError(result)
    })?
  } else {
    raw_content
  };

  Ok(content)
}

fn write_destination_content(
  job_result: JobResult,
  path: &Path,
  content: &str,
  mode: &str,
) -> Result<(), MessageError> {
  let transformed_content = if mode == "xml" {
    json_to_xml(content, None).map_err(|e| {
      MessageError::ProcessingError(
        job_result
          .clone()
          .with_status(JobStatus::Error)
          .with_message(&format!("Unable to write xml from json: {}", e)),
      )
    })?
  } else {
    content.to_owned()
  };

  fs::write(path, &transformed_content).map_err(|e| {
    MessageError::ProcessingError(
      job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&format!("Unable to write generated result: {}", e)),
    )
  })?;

  Ok(())
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn process_with_string_test_ok() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_1.json".to_string(),
      source_paths: vec!["tests/source_1.json".to_string()],
      template: ".name".to_string(),
      template_mode: "string".to_string(),
      ..Default::default()
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result);

    assert!(result.is_ok());
    assert_eq!(
      fs::read_to_string("tests/destination_1.json").unwrap(),
      "\"John Doe\"\n"
    );
  }

  #[test]
  fn process_with_template_file_test_ok() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_2.json".to_string(),
      source_paths: vec!["tests/source_1.json".to_string()],
      template: "tests/template_1.jq".to_string(),
      template_mode: "file".to_string(),
      ..Default::default()
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result);

    assert!(result.is_ok());

    assert_eq!(
      fs::read_to_string("tests/destination_2.json").unwrap(),
      "\"John Doe\"\n"
    );
  }

  #[test]
  fn process_xml_to_xml_ok() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_3.xml".to_string(),
      source_paths: vec!["tests/source_3.xml".to_string()],
      template: ".".to_string(),
      output_mode: "xml".to_string(),
      template_mode: "string".to_string(),
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result);

    assert!(result.is_ok());
    assert_eq!(
      fs::read_to_string("tests/destination_3.xml").unwrap(),
      fs::read_to_string("tests/source_3.xml").unwrap()
    );
  }

  #[test]
  fn process_xml_to_json_ok() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_4.json".to_string(),
      source_paths: vec!["tests/source_4.xml".to_string()],
      template: ".root[0].name[0][\"_\"]".to_string(),
      template_mode: "string".to_string(),
      ..Default::default()
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result);

    assert!(result.is_ok());
    assert_eq!(
      fs::read_to_string("tests/destination_4.json").unwrap(),
      "\"John Doe\"\n"
    );
  }

  #[test]
  fn process_json_to_xml_ok() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_5.xml".to_string(),
      source_paths: vec!["tests/source_5.json".to_string()],
      template: ".".to_string(),
      template_mode: "string".to_string(),
      output_mode: "xml".to_string(),
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result);

    assert!(result.is_ok());
    assert_eq!(
      fs::read_to_string("tests/destination_5.xml").unwrap(),
      r#"<name type="str">John Doe</name>"#
    );
  }

  #[test]
  fn process_test_error() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_6.json".to_string(),
      source_paths: vec!["tests/missing_source.json".to_string()],
      template: ".name".to_string(),
      template_mode: "string".to_string(),
      output_mode: "xml".to_string(),
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result.clone());

    assert!(result.is_err());

    let msg_err = result.err().unwrap();
    assert_eq!(
      msg_err,
      MessageError::ProcessingError(
        job_result
          .with_status(JobStatus::Error)
          .with_message("No such file: tests/missing_source.json")
      )
    );
  }

  #[test]
  fn mode_test_error() {
    let parameters = JsonTransformParameters {
      destination_path: "tests/destination_67json".to_string(),
      source_paths: vec!["tests/tests/source_7.json".to_string()],
      template: "".to_string(),
      template_mode: "url".to_string(),
      ..Default::default()
    };

    let job_result = JobResult::new(123);
    let result = process(None, parameters, job_result.clone());

    assert!(result.is_err());

    let msg_err = result.err().unwrap();
    assert_eq!(
      msg_err,
      MessageError::ProcessingError(
        job_result
          .with_status(JobStatus::Error)
          .with_message(r#"Mode "url" not supported."#)
      )
    );
  }
}
